import { Module } from '@nestjs/common';
import { PlanEmpresaService } from './plan-empresa.service';
import { PlanEmpresaController } from './plan-empresa.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PlanEmpresa } from '../shared/entities/PlanEmpresa';

@Module({
  imports: [TypeOrmModule.forFeature([PlanEmpresa], 'creece')],
  controllers: [PlanEmpresaController],
  providers: [PlanEmpresaService],
})
export class PlanEmpresaModule {}
