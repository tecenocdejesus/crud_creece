import { Test, TestingModule } from '@nestjs/testing';
import { InformacionGeneralController } from './informacion-general.controller';
import { InformacionGeneralService } from './informacion-general.service';

describe('InformacionGeneralController', () => {
  let controller: InformacionGeneralController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [InformacionGeneralController],
      providers: [InformacionGeneralService],
    }).compile();

    controller = module.get<InformacionGeneralController>(InformacionGeneralController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
