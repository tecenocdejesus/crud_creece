import { Test, TestingModule } from '@nestjs/testing';
import { InformacionGeneralService } from './informacion-general.service';

describe('InformacionGeneralService', () => {
  let service: InformacionGeneralService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [InformacionGeneralService],
    }).compile();

    service = module.get<InformacionGeneralService>(InformacionGeneralService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
