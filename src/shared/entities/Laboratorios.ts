import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { LaboratoriosHasInformacionEmpresa } from "./LaboratoriosHasInformacionEmpresa";

@Entity("laboratorios", { schema: "creece" })
export class Laboratorios {
  @PrimaryGeneratedColumn({ type: "int", name: "id" })
  id: number;

  @Column("int", { name: "LAB_Operaciones" })
  labOperaciones: number;

  @Column("int", { name: "LAB_Marketing_y_Comercializacion" })
  labMarketingYComercializacion: number;

  @Column("int", { name: "LAB_Oferta_de_Valor" })
  labOfertaDeValor: number;

  @Column("int", { name: "LAB_Contabilidad_y_Finanzas" })
  labContabilidadYFinanzas: number;

  @Column("int", { name: "LAB_Formalizacion" })
  labFormalizacion: number;

  @Column("int", { name: "LAB_Capital_Humano" })
  labCapitalHumano: number;

  @OneToMany(
    () => LaboratoriosHasInformacionEmpresa,
    (laboratoriosHasInformacionEmpresa) =>
      laboratoriosHasInformacionEmpresa.laboratorios
  )
  laboratoriosHasInformacionEmpresas: LaboratoriosHasInformacionEmpresa[];
}
