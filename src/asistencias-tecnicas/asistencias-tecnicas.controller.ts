import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AsistenciasTecnicasService } from './asistencias-tecnicas.service';
import { CreateAsistenciasTecnicaDto } from './dto/create-asistencias-tecnica.dto';
import { UpdateAsistenciasTecnicaDto } from './dto/update-asistencias-tecnica.dto';
@ApiTags('Asistencias Tecnicas')
@Controller('asistencias-tecnicas')
export class AsistenciasTecnicasController {
  constructor(
    private readonly asistenciasTecnicasService: AsistenciasTecnicasService,
  ) {}

  @Post()
  create(@Body() createAsistenciasTecnicaDto: CreateAsistenciasTecnicaDto) {
    return this.asistenciasTecnicasService.create(createAsistenciasTecnicaDto);
  }

  @Get()
  findAll() {
    return this.asistenciasTecnicasService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.asistenciasTecnicasService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateAsistenciasTecnicaDto: UpdateAsistenciasTecnicaDto,
  ) {
    return this.asistenciasTecnicasService.update(
      +id,
      updateAsistenciasTecnicaDto,
    );
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.asistenciasTecnicasService.remove(+id);
  }
}
