import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PlanEmpresa } from 'src/shared/entities/PlanEmpresa';
import { Repository } from 'typeorm';
import { CreatePlanEmpresaDto } from './dto/create-plan-empresa.dto';
import { UpdatePlanEmpresaDto } from './dto/update-plan-empresa.dto';

@Injectable()
export class PlanEmpresaService {
  constructor(
    @InjectRepository(PlanEmpresa, 'creece')
    private readonly repository: Repository<PlanEmpresa>,
  ) {}
  create(createPlanEmpresaDto: CreatePlanEmpresaDto) {
    return this.repository.save({ ...createPlanEmpresaDto });
  }

  findAll() {
    return this.repository.find({});
  }

  findOne(id: number) {
    return this.repository.find();
  }

  update(id: number, updatePlanEmpresaDto: UpdatePlanEmpresaDto) {
    return this.repository.update(id, updatePlanEmpresaDto);
  }

  remove(id: number) {
    return `This action removes a #${id} planEmpresa`;
  }
}
