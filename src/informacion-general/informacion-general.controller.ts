import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { InformacionGeneralService } from './informacion-general.service';
import { CreateInformacionGeneralDto } from './dto/create-informacion-general.dto';
import { UpdateInformacionGeneralDto } from './dto/update-informacion-general.dto';
import { ApiTags } from '@nestjs/swagger';
@ApiTags('Información')
@Controller('informacion-general')
export class InformacionGeneralController {
  constructor(
    private readonly informacionGeneralService: InformacionGeneralService,
  ) {}

  @Post()
  create(@Body() createInformacionGeneralDto: CreateInformacionGeneralDto) {
    return this.informacionGeneralService.create(createInformacionGeneralDto);
  }

  @Get()
  findAll() {
    return this.informacionGeneralService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.informacionGeneralService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateInformacionGeneralDto: UpdateInformacionGeneralDto,
  ) {
    return this.informacionGeneralService.update(
      +id,
      updateInformacionGeneralDto,
    );
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.informacionGeneralService.remove(+id);
  }
}
