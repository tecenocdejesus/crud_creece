import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { AsistenciasTecnicas } from "./AsistenciasTecnicas";
import { Consultor } from "./Consultor";
import { InformacionEmpresa } from "./InformacionEmpresa";

@Index(
  "fk_informacion_empresa_has_Asistencias_tecnicas_Asistencias_idx",
  ["asistenciasTecnicasId"],
  {}
)
@Index(
  "fk_informacion_empresa_has_Asistencias_tecnicas_informacion_idx",
  ["informacionEmpresaId"],
  {}
)
@Index(
  "fk_informacion_empresa_has_Asistencias_tecnicas_Consultor1_idx",
  ["consultorIdConsultor"],
  {}
)
@Entity("informacion_empresa_has_Asistencias_tecnicas", { schema: "creece" })
export class InformacionEmpresaHasAsistenciasTecnicas {
  @Column("int", { primary: true, name: "informacion_empresa_id" })
  informacionEmpresaId: number;

  @Column("int", { primary: true, name: "Asistencias_tecnicas_id" })
  asistenciasTecnicasId: number;

  @Column("int", { name: "Consultor_idConsultor" })
  consultorIdConsultor: number;

  @Column("varchar", { name: "horas", nullable: true, length: 45 })
  horas: string | null;

  @ManyToOne(
    () => AsistenciasTecnicas,
    (asistenciasTecnicas) =>
      asistenciasTecnicas.informacionEmpresaHasAsistenciasTecnicas,
    { onDelete: "NO ACTION", onUpdate: "NO ACTION" }
  )
  @JoinColumn([{ name: "Asistencias_tecnicas_id", referencedColumnName: "id" }])
  asistenciasTecnicas: AsistenciasTecnicas;

  @ManyToOne(
    () => Consultor,
    (consultor) => consultor.informacionEmpresaHasAsistenciasTecnicas,
    { onDelete: "NO ACTION", onUpdate: "NO ACTION" }
  )
  @JoinColumn([
    { name: "Consultor_idConsultor", referencedColumnName: "idConsultor" },
  ])
  consultorIdConsultor2: Consultor;

  @ManyToOne(
    () => InformacionEmpresa,
    (informacionEmpresa) =>
      informacionEmpresa.informacionEmpresaHasAsistenciasTecnicas,
    { onDelete: "NO ACTION", onUpdate: "NO ACTION" }
  )
  @JoinColumn([{ name: "informacion_empresa_id", referencedColumnName: "id" }])
  informacionEmpresa: InformacionEmpresa;
}
