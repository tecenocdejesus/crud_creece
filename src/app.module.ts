import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsuarioModule } from './usuario/usuario.module';
import { AsistenciasTecnicasModule } from './asistencias-tecnicas/asistencias-tecnicas.module';
import { GestorModule } from './gestor/gestor.module';
import { InformacionGeneralModule } from './informacion-general/informacion-general.module';
import { LaboratoriosModule } from './laboratorios/laboratorios.module';
import { PlanEmpresaModule } from './plan-empresa/plan-empresa.module';
import { AuthModule } from './auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AsistenciasTecnicas } from './shared/entities/AsistenciasTecnicas';
import { InformacionEmpresaHasAsistenciasTecnicas } from './shared/entities/InformacionEmpresaHasAsistenciasTecnicas';
import { InformacionEmpresa } from './shared/entities/InformacionEmpresa';
import { Gestor } from './shared/entities/Gestor';
import { PlanEmpresa } from './shared/entities/PlanEmpresa';
import { Deserciones } from './shared/entities/Deserciones';
import { Laboratorios } from './shared/entities/Laboratorios';
import { LaboratoriosHasInformacionEmpresa } from './shared/entities/LaboratoriosHasInformacionEmpresa';
import { Consultor } from './shared/entities/Consultor';
@Module({
  imports: [
    UsuarioModule,
    AsistenciasTecnicasModule,
    GestorModule,
    InformacionGeneralModule,
    LaboratoriosModule,
    PlanEmpresaModule,
    AuthModule,
    TypeOrmModule.forRoot({
      name: 'creece',
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'fran',
      password: '1234',
      database: 'creece',
      entities: [
        AsistenciasTecnicas,
        InformacionEmpresa,
        InformacionEmpresaHasAsistenciasTecnicas,
        Gestor,
        PlanEmpresa,
        Deserciones,
        Laboratorios,
        LaboratoriosHasInformacionEmpresa,
        Consultor,
      ],
      synchronize: false,
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
