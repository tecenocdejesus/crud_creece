import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateGestorDto } from './dto/create-gestor.dto';
import { UpdateGestorDto } from './dto/update-gestor.dto';
import { Gestor } from '../shared/entities/Gestor';

@Injectable()
export class GestorService {
  constructor(
    @InjectRepository(Gestor, 'creece')
    private readonly repository: Repository<Gestor>,
  ) {}
  create(createGestorDto: CreateGestorDto) {
    return this.repository.save({ ...createGestorDto });
  }

  findAll() {
    return this.repository.find({});
  }

  findOne(id: number) {
    return this.repository.find();
  }

  update(id: number, updateGestorDto: UpdateGestorDto) {
    return this.repository.update(id, updateGestorDto);
  }

  remove(id: number) {
    return `This action removes a #${id} asistenciasTecnica`;
  }
}
