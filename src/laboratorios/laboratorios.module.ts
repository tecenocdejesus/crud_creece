import { Module } from '@nestjs/common';
import { LaboratoriosService } from './laboratorios.service';
import { LaboratoriosController } from './laboratorios.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Laboratorios } from '../shared/entities/Laboratorios';

@Module({
  imports: [TypeOrmModule.forFeature([Laboratorios], 'creece')],
  controllers: [LaboratoriosController],
  providers: [LaboratoriosService],
})
export class LaboratoriosModule {}
