import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { InformacionEmpresa } from './InformacionEmpresa';

@Entity('Plan_empresa', { schema: 'creece' })
export class PlanEmpresa {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
  id: number;

  @Column('int', { name: 'pe_aprobacion' })
  peAprobacion: number;

  @OneToMany(
    () => InformacionEmpresa,
    (informacionEmpresa) => informacionEmpresa.planEmpresa,
  )
  informacionEmpresas: InformacionEmpresa[];
}
