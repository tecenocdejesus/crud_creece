import { Test, TestingModule } from '@nestjs/testing';
import { AsistenciasTecnicasController } from './asistencias-tecnicas.controller';
import { AsistenciasTecnicasService } from './asistencias-tecnicas.service';

describe('AsistenciasTecnicasController', () => {
  let controller: AsistenciasTecnicasController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AsistenciasTecnicasController],
      providers: [AsistenciasTecnicasService],
    }).compile();

    controller = module.get<AsistenciasTecnicasController>(AsistenciasTecnicasController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
