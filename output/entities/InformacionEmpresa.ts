import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Gestor } from './Gestor';
import { PlanEmpresa } from './PlanEmpresa';
import { Deserciones } from './Deserciones';
import { InformacionEmpresaHasAsistenciasTecnicas } from './InformacionEmpresaHasAsistenciasTecnicas';
import { LaboratoriosHasInformacionEmpresa } from './LaboratoriosHasInformacionEmpresa';

@Index('NIT_Empresa_UNIQUE', ['nitEmpresa'], { unique: true })
@Index('Correo_Electronico_UNIQUE', ['correoElectronico'], { unique: true })
@Index('Nombre_Comercial_UNIQUE', ['nombreComercial'], { unique: true })
@Index('Doc_Identidad_Empresario_UNIQUE', ['docIdentidadEmpresario'], {
  unique: true,
})
@Index('fk_informacion_empresa_Gestor_idx', ['gestorIdGestor'], {})
@Index('fk_informacion_empresa_deserciones1_idx', ['desercionesId'], {})
@Index('fk_informacion_empresa_Plan_empresa1_idx', ['planEmpresaId'], {})
@Entity('informacion_empresa', { schema: 'creece' })
export class InformacionEmpresa {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
  id: number;

  @Column('int', { name: 'NIT_Empresa', unique: true })
  nitEmpresa: number;

  @Column('varchar', { name: 'Nombre_Comercial', unique: true, length: 45 })
  nombreComercial: string;

  @Column('varchar', { name: 'Contacto', nullable: true, length: 45 })
  contacto: string | null;

  @Column('varchar', { name: 'Correo_Electronico', unique: true, length: 45 })
  correoElectronico: string;

  @Column('varchar', { name: 'Direccion', nullable: true, length: 45 })
  direccion: string | null;

  @Column('varchar', { name: 'Ciudad', nullable: true, length: 45 })
  ciudad: string | null;

  @Column('varchar', { name: 'Nombre_Empresario', nullable: true, length: 45 })
  nombreEmpresario: string | null;

  @Column('varchar', {
    name: 'Doc_Identidad_Empresario',
    nullable: true,
    unique: true,
    length: 45,
  })
  docIdentidadEmpresario: string | null;

  @Column('int', { name: 'Gestor_idGestor' })
  gestorIdGestor: number;

  @Column('int', { name: 'deserciones_id' })
  desercionesId: number;

  @Column('int', { name: 'Plan_empresa_id' })
  planEmpresaId: number;

  @ManyToOne(() => Gestor, (gestor) => gestor.informacionEmpresas, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION',
  })
  @JoinColumn([{ name: 'Gestor_idGestor', referencedColumnName: 'idGestor' }])
  gestorIdGestor2: Gestor;

  @ManyToOne(
    () => PlanEmpresa,
    (planEmpresa) => planEmpresa.informacionEmpresas,
    { onDelete: 'NO ACTION', onUpdate: 'NO ACTION' },
  )
  @JoinColumn([{ name: 'Plan_empresa_id', referencedColumnName: 'id' }])
  planEmpresa: PlanEmpresa;

  @ManyToOne(
    () => Deserciones,
    (deserciones) => deserciones.informacionEmpresas,
    { onDelete: 'NO ACTION', onUpdate: 'NO ACTION' },
  )
  @JoinColumn([{ name: 'deserciones_id', referencedColumnName: 'id' }])
  deserciones: Deserciones;

  @OneToMany(
    () => InformacionEmpresaHasAsistenciasTecnicas,
    (informacionEmpresaHasAsistenciasTecnicas) =>
      informacionEmpresaHasAsistenciasTecnicas.informacionEmpresa,
  )
  informacionEmpresaHasAsistenciasTecnicas: InformacionEmpresaHasAsistenciasTecnicas[];

  @OneToMany(
    () => LaboratoriosHasInformacionEmpresa,
    (laboratoriosHasInformacionEmpresa) =>
      laboratoriosHasInformacionEmpresa.informacionEmpresa,
  )
  laboratoriosHasInformacionEmpresas: LaboratoriosHasInformacionEmpresa[];
}
