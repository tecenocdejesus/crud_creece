import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, MinLength } from 'class-validator';
export class CreateGestorDto {
  @IsNotEmpty()
  @MinLength(1)
  @IsString()
  @ApiProperty({
    description: 'Nombre del gestor',
    default: 'Gestor1',
  })
  nombre: string;

  @ApiProperty({
    description: 'Correo del gestor',
    default: 'Gestor1@mail.com',
  })
  correoElectronico: string;

  @ApiProperty({
    description: 'documento de identidad del gestor',
    default: '1234567890',
  })
  docIdentidad: string;

  @ApiProperty({
    description: 'telefono del gestor',
    default: '001222334455ß',
  })
  telefono: string;
}
