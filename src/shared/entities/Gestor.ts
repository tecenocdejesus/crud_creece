import {
  Column,
  Entity,
  Index,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { InformacionEmpresa } from './InformacionEmpresa';

@Index('Doc_Identidad_UNIQUE', ['docIdentidad'], { unique: true })
@Entity('Gestor', { schema: 'creece' })
export class Gestor {
  @PrimaryGeneratedColumn({ type: 'int', name: 'idGestor' })
  idGestor: number;

  @Column('varchar', { name: 'nombre', nullable: true, length: 145 })
  nombre: string | null;

  @Column('varchar', {
    name: 'Correo_Electronico',
    nullable: true,
    length: 300,
  })
  correoElectronico: string | null;

  @Column('varchar', { name: 'Doc_Identidad', unique: true, length: 45 })
  docIdentidad: string;

  @Column('varchar', { name: 'telefono', nullable: true, length: 45 })
  telefono: string | null;

  @OneToMany(
    () => InformacionEmpresa,
    (informacionEmpresa) => informacionEmpresa.gestorIdGestor2,
  )
  informacionEmpresas: InformacionEmpresa[];
}
