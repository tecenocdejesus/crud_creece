import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { InformacionEmpresa } from "./InformacionEmpresa";
import { Laboratorios } from "./Laboratorios";

@Index(
  "fk_laboratorios_has_informacion_empresa_informacion_empresa_idx",
  ["informacionEmpresaId"],
  {}
)
@Index(
  "fk_laboratorios_has_informacion_empresa_laboratorios1_idx",
  ["laboratoriosId"],
  {}
)
@Entity("laboratorios_has_informacion_empresa", { schema: "creece" })
export class LaboratoriosHasInformacionEmpresa {
  @Column("int", { primary: true, name: "laboratorios_id" })
  laboratoriosId: number;

  @Column("int", { primary: true, name: "informacion_empresa_id" })
  informacionEmpresaId: number;

  @Column("varchar", { name: "horas", nullable: true, length: 45 })
  horas: string | null;

  @ManyToOne(
    () => InformacionEmpresa,
    (informacionEmpresa) =>
      informacionEmpresa.laboratoriosHasInformacionEmpresas,
    { onDelete: "NO ACTION", onUpdate: "NO ACTION" }
  )
  @JoinColumn([{ name: "informacion_empresa_id", referencedColumnName: "id" }])
  informacionEmpresa: InformacionEmpresa;

  @ManyToOne(
    () => Laboratorios,
    (laboratorios) => laboratorios.laboratoriosHasInformacionEmpresas,
    { onDelete: "NO ACTION", onUpdate: "NO ACTION" }
  )
  @JoinColumn([{ name: "laboratorios_id", referencedColumnName: "id" }])
  laboratorios: Laboratorios;
}
