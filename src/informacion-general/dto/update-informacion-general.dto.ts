import { PartialType } from '@nestjs/mapped-types';
import { CreateInformacionGeneralDto } from './create-informacion-general.dto';

export class UpdateInformacionGeneralDto extends PartialType(CreateInformacionGeneralDto) {}
