import { Test, TestingModule } from '@nestjs/testing';
import { PlanEmpresaController } from './plan-empresa.controller';
import { PlanEmpresaService } from './plan-empresa.service';

describe('PlanEmpresaController', () => {
  let controller: PlanEmpresaController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PlanEmpresaController],
      providers: [PlanEmpresaService],
    }).compile();

    controller = module.get<PlanEmpresaController>(PlanEmpresaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
