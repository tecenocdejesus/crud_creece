import { PartialType } from '@nestjs/mapped-types';
import { CreatePlanEmpresaDto } from './create-plan-empresa.dto';

export class UpdatePlanEmpresaDto extends PartialType(CreatePlanEmpresaDto) {}
