import { PartialType } from '@nestjs/mapped-types';
import { CreateAsistenciasTecnicaDto } from './create-asistencias-tecnica.dto';

export class UpdateAsistenciasTecnicaDto extends PartialType(CreateAsistenciasTecnicaDto) {}
