import { Module } from '@nestjs/common';
import { InformacionGeneralService } from './informacion-general.service';
import { InformacionGeneralController } from './informacion-general.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { InformacionEmpresa } from '../shared/entities/InformacionEmpresa';

@Module({
  imports: [TypeOrmModule.forFeature([InformacionEmpresa], 'creece')],
  controllers: [InformacionGeneralController],
  providers: [InformacionGeneralService],
})
export class InformacionGeneralModule {}
