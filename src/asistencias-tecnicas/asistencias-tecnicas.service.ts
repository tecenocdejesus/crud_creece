import { Injectable } from '@nestjs/common';
import { CreateAsistenciasTecnicaDto } from './dto/create-asistencias-tecnica.dto';
import { UpdateAsistenciasTecnicaDto } from './dto/update-asistencias-tecnica.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { AsistenciasTecnicas } from '../shared/entities/AsistenciasTecnicas';
import { Repository } from 'typeorm';

@Injectable()
export class AsistenciasTecnicasService {
  constructor(
    @InjectRepository(AsistenciasTecnicas, 'creece')
    private readonly repository: Repository<AsistenciasTecnicas>,
  ) {}
  create(createAsistenciasTecnicaDto: CreateAsistenciasTecnicaDto) {
    return this.repository.save({ ...createAsistenciasTecnicaDto });
  }

  findAll() {
    return this.repository.find({});
  }

  findOne(id: number) {
    return this.repository.find();
  }

  update(id: number, updateAsistenciasTecnicaDto: UpdateAsistenciasTecnicaDto) {
    return this.repository.update(id, updateAsistenciasTecnicaDto);
  }

  remove(id: number) {
    return `This action removes a #${id} asistenciasTecnica`;
  }
}
