import { ApiProperty } from '@nestjs/swagger';

export class CreateInformacionGeneralDto {
  @ApiProperty({
    description: 'Nit o Rut de la empresa',
    default: 1234567890,
  })
  nitEmpresa: number;
  @ApiProperty({
    description: 'Nombre de la empresa',
    default: 'miempresa',
  })
  nombreComercial: string;
  @ApiProperty({
    description: 'Numero de contacto de la empresa',
    default: '1234567890',
  })
  contacto: string;
  @ApiProperty({
    description: 'Correo de la empresa',
    default: 'micorreo@mail.com',
  })
  correoElectronico: string;
  @ApiProperty({
    description: 'Direccion de la empresa',
    default: 'calle 23 # 25-43',
  })
  direccion: string;
  @ApiProperty({
    description: 'ciudad de la empresa',
    default: 'Ramos Mejia',
  })
  ciudad: string;

  @ApiProperty({
    description: 'Nombre del empresario - representante',
    default: 'gerardo jimenez',
  })
  nombreEmpresario: string;
  @ApiProperty({
    description: 'documento de identidad del empresario',
    default: '1234567890',
  })
  docIdentidadEmpresario: string;
}
