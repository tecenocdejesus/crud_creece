import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { InformacionEmpresaHasAsistenciasTecnicas } from './InformacionEmpresaHasAsistenciasTecnicas';

@Entity('Asistencias_tecnicas', { schema: 'creece' })
export class AsistenciasTecnicas {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
  id: number;

  @Column('int', { name: 'AT_Operaciones' })
  atOperaciones: number;

  @Column('int', { name: 'AT_Marketing_y_Comercializacion' })
  atMarketingYComercializacion: number;

  @Column('int', { name: 'AT_Oferta_de_Valor' })
  atOfertaDeValor: number;

  @Column('int', { name: 'AT_Contabilidad_y_Finanzas' })
  atContabilidadYFinanzas: number;

  @Column('int', { name: 'AT_Formalizacion' })
  atFormalizacion: number;

  @Column('int', { name: 'AT_Capital_Humano' })
  atCapitalHumano: number;

  @Column('int', { name: 'AT_Seg_Plan_de_Empresa' })
  atSegPlanDeEmpresa: number;

  @OneToMany(
    () => InformacionEmpresaHasAsistenciasTecnicas,
    (informacionEmpresaHasAsistenciasTecnicas) =>
      informacionEmpresaHasAsistenciasTecnicas.asistenciasTecnicas,
  )
  informacionEmpresaHasAsistenciasTecnicas: InformacionEmpresaHasAsistenciasTecnicas[];
}
