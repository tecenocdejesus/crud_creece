import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Laboratorios } from 'src/shared/entities/Laboratorios';
import { Repository } from 'typeorm';
import { CreateLaboratorioDto } from './dto/create-laboratorio.dto';
import { UpdateLaboratorioDto } from './dto/update-laboratorio.dto';

@Injectable()
export class LaboratoriosService {
  constructor(
    @InjectRepository(Laboratorios, 'creece')
    private readonly repository: Repository<Laboratorios>,
  ) {}
  create(createLaboratorioDto: CreateLaboratorioDto) {
    return this.repository.save({ ...createLaboratorioDto });
  }

  findAll() {
    return this.repository.find({});
  }

  findOne(id: number) {
    return this.repository.find();
  }

  update(id: number, updateLaboratorioDto: UpdateLaboratorioDto) {
    return this.repository.update(id, updateLaboratorioDto);
  }

  remove(id: number) {
    return `This action removes a #${id} laboratorio`;
  }
}
