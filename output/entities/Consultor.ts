import {
  Column,
  Entity,
  Index,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { InformacionEmpresaHasAsistenciasTecnicas } from "./InformacionEmpresaHasAsistenciasTecnicas";

@Index("Consultorcol_UNIQUE", ["consultorcol"], { unique: true })
@Entity("Consultor", { schema: "creece" })
export class Consultor {
  @PrimaryGeneratedColumn({ type: "int", name: "idConsultor" })
  idConsultor: number;

  @Column("varchar", { name: "nombre", nullable: true, length: 45 })
  nombre: string | null;

  @Column("varchar", { name: "apellido", nullable: true, length: 45 })
  apellido: string | null;

  @Column("varchar", { name: "Telefono", nullable: true, length: 45 })
  telefono: string | null;

  @Column("varchar", { name: "Emaiil", nullable: true, length: 245 })
  emaiil: string | null;

  @Column("varchar", { name: "Consultorcol", unique: true, length: 45 })
  consultorcol: string;

  @OneToMany(
    () => InformacionEmpresaHasAsistenciasTecnicas,
    (informacionEmpresaHasAsistenciasTecnicas) =>
      informacionEmpresaHasAsistenciasTecnicas.consultorIdConsultor2
  )
  informacionEmpresaHasAsistenciasTecnicas: InformacionEmpresaHasAsistenciasTecnicas[];
}
