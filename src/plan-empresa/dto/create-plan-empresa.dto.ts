import { ApiProperty } from '@nestjs/swagger';

export class CreatePlanEmpresaDto {
  @ApiProperty({
    description: 'Aprobacion plan de empresa 1-Si 0-No',
    default: 1,
  })
  peAprobacion: number;
}
