import { ApiProperty } from '@nestjs/swagger';

export class CreateAsistenciasTecnicaDto {
  @ApiProperty({
    description: 'Asistencia Operaciones',
    default: 1,
  })
  atOperaciones: number;

  @ApiProperty({
    description: 'Asistencia Tecnica Marketing y comercializacion',
    default: 1,
  })
  atMarketingYComercializacion: number;

  @ApiProperty({
    description: 'Asistencia tecnica oferta de valor',
    default: 2,
  })
  atOfertaDeValor: number;

  @ApiProperty({
    description: 'Asistencia tecnica contabilidad y finanzas',
    default: 2,
  })
  atContabilidadYFinanzas: number;

  @ApiProperty({
    description: 'Asistencia tecnica fromalizacion',
    default: 1,
  })
  atFormalizacion: number;

  @ApiProperty({
    description: 'Asistencia tecnica capital humano',
    default: 1,
  })
  atCapitalHumano: number;

  @ApiProperty({
    description: 'Asistencia tecnica plan de empresa',
    default: 2,
  })
  atSegPlanDeEmpresa: number;
}
