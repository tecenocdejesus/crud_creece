import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { InformacionEmpresa } from 'src/shared/entities/InformacionEmpresa';
import { Repository } from 'typeorm';
import { CreateInformacionGeneralDto } from './dto/create-informacion-general.dto';
import { UpdateInformacionGeneralDto } from './dto/update-informacion-general.dto';

@Injectable()
export class InformacionGeneralService {
  constructor(
    @InjectRepository(InformacionEmpresa, 'creece')
    private readonly repository: Repository<InformacionEmpresa>,
  ) {}
  create(createInformacionGeneralDto: CreateInformacionGeneralDto) {
    return this.repository.save({ ...createInformacionGeneralDto });
  }

  findAll() {
    return this.repository.find({});
  }

  findOne(id: number) {
    return this.repository.find();
  }

  update(id: number, updateInformacionGeneralDto: UpdateInformacionGeneralDto) {
    return this.repository.update(id, updateInformacionGeneralDto);
  }

  remove(id: number) {
    return `This action removes a #${id} informacionGeneral`;
  }
}
