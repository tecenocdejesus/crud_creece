import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { InformacionEmpresa } from "./InformacionEmpresa";

@Entity("deserciones", { schema: "creece" })
export class Deserciones {
  @PrimaryGeneratedColumn({ type: "int", name: "id" })
  id: number;

  @Column("int", { name: "EstadoDeMiCroempresa" })
  estadoDeMiCroempresa: number;

  @Column("varchar", { name: "Motivo_de_Desercion", length: 45 })
  motivoDeDesercion: string;

  @Column("varchar", { name: "Etapa_de_Desercion", nullable: true, length: 45 })
  etapaDeDesercion: string | null;

  @OneToMany(
    () => InformacionEmpresa,
    (informacionEmpresa) => informacionEmpresa.deserciones
  )
  informacionEmpresas: InformacionEmpresa[];
}
