import { Module } from '@nestjs/common';
import { GestorService } from './gestor.service';
import { GestorController } from './gestor.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Gestor } from '../shared/entities/Gestor';

@Module({
  imports: [TypeOrmModule.forFeature([Gestor], 'creece')],
  controllers: [GestorController],
  providers: [GestorService],
})
export class GestorModule {}
