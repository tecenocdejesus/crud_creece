import { Test, TestingModule } from '@nestjs/testing';
import { PlanEmpresaService } from './plan-empresa.service';

describe('PlanEmpresaService', () => {
  let service: PlanEmpresaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PlanEmpresaService],
    }).compile();

    service = module.get<PlanEmpresaService>(PlanEmpresaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
