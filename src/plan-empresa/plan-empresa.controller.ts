import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { PlanEmpresaService } from './plan-empresa.service';
import { CreatePlanEmpresaDto } from './dto/create-plan-empresa.dto';
import { UpdatePlanEmpresaDto } from './dto/update-plan-empresa.dto';
import { ApiTags } from '@nestjs/swagger';
@ApiTags('Plan Empresa')
@Controller('plan-empresa')
export class PlanEmpresaController {
  constructor(private readonly planEmpresaService: PlanEmpresaService) {}

  @Post()
  create(@Body() createPlanEmpresaDto: CreatePlanEmpresaDto) {
    return this.planEmpresaService.create(createPlanEmpresaDto);
  }

  @Get()
  findAll() {
    return this.planEmpresaService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.planEmpresaService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updatePlanEmpresaDto: UpdatePlanEmpresaDto,
  ) {
    return this.planEmpresaService.update(+id, updatePlanEmpresaDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.planEmpresaService.remove(+id);
  }
}
