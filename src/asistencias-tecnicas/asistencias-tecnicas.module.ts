import { Module } from '@nestjs/common';
import { AsistenciasTecnicasService } from './asistencias-tecnicas.service';
import { AsistenciasTecnicasController } from './asistencias-tecnicas.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AsistenciasTecnicas } from '../shared/entities/AsistenciasTecnicas';
import { InformacionEmpresaHasAsistenciasTecnicas } from '../shared/entities/InformacionEmpresaHasAsistenciasTecnicas';

@Module({
  imports: [
    TypeOrmModule.forFeature(
      [AsistenciasTecnicas, InformacionEmpresaHasAsistenciasTecnicas],
      'creece',
    ),
  ],
  controllers: [AsistenciasTecnicasController],
  providers: [AsistenciasTecnicasService],
})
export class AsistenciasTecnicasModule {}
