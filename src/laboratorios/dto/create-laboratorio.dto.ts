import { ApiProperty } from '@nestjs/swagger';

export class CreateLaboratorioDto {
  @ApiProperty({
    description: 'Horas operaciones',
    default: 4,
  })
  labOperaciones: number;

  @ApiProperty({
    description: 'Horas marketing y comercializacion',
    default: 4,
  })
  labMarketingYComercializacion: number;

  @ApiProperty({
    description: 'Horas oferta de valor',
    default: 3,
  })
  labOfertaDeValor: number;

  @ApiProperty({
    description: 'Horas contablidad y finanzas',
    default: 3,
  })
  labContabilidadYFinanzas: number;

  @ApiProperty({
    description: 'Horas formalizacion',
    default: 3,
  })
  labFormalizacion: number;

  @ApiProperty({
    description: 'Horas capital humano',
    default: 4,
  })
  labCapitalHumano: number;
}
