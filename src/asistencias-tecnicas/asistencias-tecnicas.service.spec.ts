import { Test, TestingModule } from '@nestjs/testing';
import { AsistenciasTecnicasService } from './asistencias-tecnicas.service';

describe('AsistenciasTecnicasService', () => {
  let service: AsistenciasTecnicasService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AsistenciasTecnicasService],
    }).compile();

    service = module.get<AsistenciasTecnicasService>(AsistenciasTecnicasService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
